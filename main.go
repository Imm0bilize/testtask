package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

func makeConvert(data interface{}) map[string]interface{} {
	out := make(map[string]interface{})

	for k, v := range data.(map[string]interface{}) {
		mapInValue, ok := v.(map[string]interface{})
		if ok {
			newMap := makeConvert(mapInValue)
			for internalKey, internalValue := range newMap {
				out[fmt.Sprintf("%s.%s", k, internalKey)] = internalValue
			}
		} else {
			out[k] = v
		}
	}
	return out
}

func fileProcessing(inputFilePath, outputFilePath string) {
	inputFileBody, err := os.ReadFile(inputFilePath)
	if err != nil {
		log.Fatalf("error when reading the input data file: %s", err.Error())
	}

	var in interface{}
	if err = json.Unmarshal(inputFileBody, &in); err != nil {
		log.Fatalf("error while unmarshalling input file: %s", err.Error())
	}

	convertedData := makeConvert(in)
	out, err := json.Marshal(&convertedData)
	if err != nil {
		log.Fatalf("error while marshalling converted data: %s", err.Error())
	}
	f, err := os.Create(outputFilePath)
	if err != nil {
		log.Fatalf("error when creating output file on path <%s>: %s", outputFilePath, err.Error())
	}
	defer f.Close()
	_, err = f.Write(out)
	if err != nil {
		log.Fatalf("error when writing to the created file: %s", err.Error())
	}

}

func main() {
	var (
		inputFilePath  string
		outputFilePath string
	)
	flag.StringVar(&inputFilePath, "i", "", "path to input file (.json)")
	flag.StringVar(&outputFilePath, "o", "", "path to output file (.json)")
	flag.Parse()
	if len(inputFilePath) == 0 {
		log.Fatal("path to the input file is empty")
	}
	if len(outputFilePath) == 0 {
		log.Fatal("path to the output file is empty")
	}
	fileProcessing(inputFilePath, outputFilePath)
}
