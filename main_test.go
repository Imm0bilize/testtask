package main

import (
	"os"
	"reflect"
	"testing"
)

func TestProcessingFile(t *testing.T) {
	testTable := []struct {
		name           string
		inputFilePath  string
		outputFilePath string
	}{
		{
			name:           "default test file",
			inputFilePath:  "./assets/test1_in.json",
			outputFilePath: "./assets/test1_out.json",
		},
		{
			name:           "file with boolean values",
			inputFilePath:  "./assets/test2_in.json",
			outputFilePath: "./assets/test2_out.json",
		},
	}

	const tmpFilePath = "tmp.json"
	for _, testCase := range testTable {
		t.Run(testCase.name, func(t *testing.T) {
			fileProcessing(testCase.inputFilePath, tmpFilePath)

			expected, err := os.ReadFile(testCase.outputFilePath)
			if err != nil {
				t.Fatal(err)
			}

			actual, err := os.ReadFile(tmpFilePath)
			if err != nil {
				t.Fatal(err)
			}

			if err = os.Remove(tmpFilePath); err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(expected, actual) {
				t.Errorf("expected %s, got %s", string(expected), string(actual))
			}
		})
	}
}
